import setuptools

with open("README.md", "r", encoding="utf-8") as fh:
    long_description = fh.read()

setuptools.setup(
    name="hylleraas",
    version="0.0.1",
    author="Audun Skau Hansen, Simen Sommerfelt Reine, Tilmann Bodenstein",
    author_email="a.s.hansen@kjemi.uio.no",
    description="Framework for computational chemistry",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://github.com/",
    project_urls={
        "Bug Tracker": "https://github.com/",
    },
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    package_dir={"": "hylleraas"},
    packages=setuptools.find_packages(where="hylleraas"),
    python_requires=">=3.6",
)
