# Hylleraas Platform for JupyterHub

FROM daltonproject/daltonproject

# Hylleraas platform
WORKDIR /
RUN git clone https://gitlab.com/hylleraasplatform/hylleraas.git
WORKDIR /hylleraas
RUN pip install .

# other stuff
RUN pip install jupyter pandas llvmlite==0.31.0 numba

WORKDIR /root/

